
FROM    centos:8

# Enable EPEL for Node.js
RUN curl -sL https://rpm.nodesource.com/setup_14.x |  bash -
# Install Node.js and npm
RUN  yum install nodejs -y 
WORKDIR /app
COPY . .

RUN npm install

CMD ["npm", "start"]
EXPOSE 3000