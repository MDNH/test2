const mongoose = require('mongoose');


let server  = process.env["ME_CONFIG_MONGODB_SERVER"]; 
let user = process.env["ME_CONFIG_MONGODB_ADMINUSERNAME"];
let password = process.env["ME_CONFIG_MONGODB_ADMINPASSWORD"];

mongoose.connect(`mongodb://${user}:${password}@${server}:27017/company`, { useNewUrlParser: true }, (err) => {
    if (!err) { console.log('MongoDB Connection Succeeded.') }
    else { console.log('Error in DB connection : ' + err) }
});

require('./employee.model');
